<?php

class TaleoPage extends Page
{
    private static $singular_name = 'Taleo Page';
    private static $plural_name = 'Taleo Pages';
    private static $description = 'Show a list of available jobs';

    private static $db = array(
        'ShowRelatedJobs' => 'Boolean'
    );
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Main', CheckboxField::create('ShowRelatedJobs', 'Show related jobs when viewing an individual job'));
        return $fields;
    }
    
}

class TaleoPage_Controller extends Page_Controller
{

    private $jobData = false;

    private static $allowed_actions = [
        'ajax',
        'jobDetails'
    ];

	private static $url_handlers = array(
		'details//$ContestNumber!' => 'jobDetails'
    );

    public function ajax() {
        return $this->renderWith('JobList');
    }
    
    public function Jobs() {        
        $type = $this->request->getVar('type');
        $category = $this->request->getVar('category');
        $subcategory = $this->request->getVar('subcategory');

        $controller = new TaleoJobController;
        $jobs = $controller->getJobs($this->request->getVar('keyword'), $type, $category, $subcategory, $this->request->getVar('location'));

        return $jobs;
    }

    public function PaginatedJobs() {
        $paginatedList = new PaginatedList($this->Jobs(), $this->request);
        $paginatedList->setPageLength(12);
        return $paginatedList;
    }


    public function jobDetails($request) {
        if ($contestNumber = $request->param('ContestNumber')) {
            if ($this->jobData = TaleoJob::get()->filter('ContestNumber', $contestNumber)->first()) {
                return $this;
            }
        }
        return $this->httpError(404);
    }

    public function Job() {
        return $this->jobData;
    }




}