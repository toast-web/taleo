<?php
class TaleoJobsAdmin extends ModelAdmin {
	
	private static $url_segment = 'taleo-jobs';
	
	private static $menu_title = 'Taleo Jobs';
	
	private static $managed_models = array(
		'TaleoJobCategory',
		'TaleoJobSubCategory',
		'TaleoJob'
	);

	static $model_importers = array(
		'TaleoJobCategory' => 'TaleoJobCategoryCsvBulkLoader', 
		'TaleoJobSubCategory' => 'TaleoJobSubCategoryCsvBulkLoader'
	 );	
	
}


class TaleoJobCategoryCsvBulkLoader extends CsvBulkLoader {
	
	public $columnMap = array(
	   'CategoryType' => 'CategoryType', 
	   'Title' => 'Title',
	);

 }

 class TaleoJobSubCategoryCsvBulkLoader extends CsvBulkLoader {
	
	public $columnMap = array(
	   'CategoryType' => 'CategoryType', 
	   'Title' => 'Title',
	   'Parent' => 'Parent.Title'
	);

	public $relationCallbacks = array(
		'Parent.Title' => array(
		   'relationname' => 'Parent',
		   'callback' => 'getCategoryByTitle'
		)
	);	

	public static function getCategoryByTitle(&$obj, $val, $record) {
		return TaleoJobCategory::get()->filter('Title', $val)->First();
	 }	

 }