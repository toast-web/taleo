<?php

class Taleo {

    private static $api_username;

    private static $api_password;

    private static $api_url;

    private static $api_action;

    private static $board_number;

    private $apiUsername = false;

    private $apiPassword = false;

    private $pagingSize = 50;

    private $pagingIndex = 1;

    private $apiUrl = 'https://adhbrac.taleo.net/enterprise/soap?ServiceName=FindService';

    private $soapAction = 'http://www.taleo.com/ws/tee800/2009/01/find/FindService#findPartialEntities';

    private $filtersAnd = [];

    private $filtersOr = [];

    private $filtersAndOr = [];

    private $sortAscending = 'false';

    private $sortField = 'CreationDate';

    private $projectionPaths = [
        'Requisition,Number' => [
            'key' => 'RequisitionNumber',
            'title' => 'Requisition number',
            'xmlPath' => 'Requisition.Requisition.Number'
        ],
        'Requisition,ContestNumber' => [
            'key' => 'ContestNumber',
            'title' => 'Contest number',
            'xmlPath' => 'Requisition.Requisition.ContestNumber'
        ],
        'CreationDate' => [
            'key' => 'CreationDate',
            'title' => 'Creation date',
            'xmlPath' => 'CreationDate'
        ],
        'CloseDate' => [
            'key' => 'CloseDate',
            'title' => 'Close date',
            'xmlPath' => 'CloseDate'
        ],
        'OpenDate' => [
            'key' => 'OpenDate',
            'title' => 'Open date',
            'xmlPath' => 'OpenDate'
        ],
        'JobBoard,JobBoard.Number' => [
            'key' => 'JobBoardNumber',
            'title' => 'Job board number',
            'xmlPath' => 'JobBoard.JobBoard.Number'
        ],
        'Requisition,State,RequisitionState.Description' => [
            'key' => 'RequisitionState',
            'title' => 'Requisition state',
            'xmlPath' => 'Requisition.Requisition.State.RequisitionState.Description.value.@content'
        ],
        'Requisition,State,RequisitionState.Number' => [
            'key' => 'RequisitionStateNumber',
            'title' => 'Requisition state number',
            'xmlPath' => 'Requisition.Requisition.State.RequisitionState.Number'
        ],
        'SourcingRequestStatus,SourcingRequestStatus.Description' => [
            'key' => 'SourcingRequestStatus',
            'title' => 'Sourcing request status',
            'xmlPath' => 'SourcingRequestStatus.SourcingRequestStatus.StatusDescription.value.@content'
        ],
        'SourcingRequestStatus,SourcingRequestStatus.Number' => [
            'key' => 'SourcingRequestNumber',
            'title' => 'Sourcing request number',
            'xmlPath' => 'SourcingRequestStatus.SourcingRequestStatus.Number'
        ],
        'Requisition,JobInformation,Title' => [
            'key' => 'Title',
            'title' => 'Title',
            'xmlPath' => 'Requisition.Requisition.JobInformation.JobInformation.Title.value.@content'
        ],
        'Requisition,JobInformation,JobField,Code' => [
            'key' => 'JobFieldCode',
            'title' => 'Job field code',
            'xmlPath' => 'Requisition.Requisition.JobInformation.JobInformation.JobField.CieJobField.Code'
        ],
        'Requisition,JobInformation,JobField,Name' => [
            'key' => 'JobFieldName',
            'title' => 'Job field name',
            'xmlPath' => 'Requisition.Requisition.JobInformation.JobInformation.JobField.CieJobField.Name.value.@content'
        ],
        'Requisition,JobInformation,JobField,Level' => [
            'key' => 'JobFieldLevel',
            'title' => 'Job field level',
            'xmlPath' => 'Requisition.Requisition.JobInformation.JobInformation.JobField.CieJobField.Level'
        ],
        'Requisition,JobInformation,JobField,Parent,Name' => [
            'key' => 'JobFieldParentName',
            'title' => 'Job field parent name',
            'xmlPath' => 'Requisition.Requisition.JobInformation.JobInformation.JobField.CieJobField.Parent.CieJobField.Name.value.@content'
        ],
        'Requisition,JobInformation,Organization,Level' => [
            'key' => 'OrganizationLevel',
            'title' => 'Organization level',
            'xmlPath' => 'Requisition.Requisition.JobInformation.JobInformation.Organization.Organization.Level'
        ],
        'Requisition,JobInformation,Organization,Code' => [
            'key' => 'OrganizationCode',
            'title' => 'Organization code',
            'xmlPath' => 'Requisition.Requisition.JobInformation.JobInformation.Organization.Organization.Code'
        ],
        'Requisition,JobInformation,Organization,Name' => [
            'key' => 'OrganizationName',
            'title' => 'Organization name',
            'xmlPath' => 'Requisition.Requisition.JobInformation.JobInformation.Organization.Organization.Name.value.@content'
        ],
        'Requisition,JobInformation,PrimaryLocation,Code' => [
            'key' => 'LocationCode',
            'title' => 'Location code',
            'xmlPath' => 'Requisition.Requisition.JobInformation.JobInformation.PrimaryLocation.CieLocation.Code'
        ],
        'Requisition,JobInformation,PrimaryLocation,Name' => [
            'key' => 'LocationName',
            'title' => 'Location name',
            'xmlPath' => 'Requisition.Requisition.JobInformation.JobInformation.PrimaryLocation.CieLocation.Name.value.@content'
        ],
        'Requisition,JobInformation,PrimaryLocation,Parent,Name' => [
            'key' => 'LocationParentName',
            'title' => 'Location parent name',
            'xmlPath' => 'Requisition.Requisition.JobInformation.JobInformation.PrimaryLocation.CieLocation.Parent.CieLocation.Name.value.@content'
        ],
        'Requisition,JobInformation,DescriptionExternalHTML' => [
            'key' => 'Description',
            'title' => 'Description',
            'xmlPath' => 'Requisition.Requisition.JobInformation.JobInformation.DescriptionExternalHTML.value.@content'
        ],
        'JobBoard,JobboardType,Description' => [
            'key' => 'JobBoardType',
            'title' => 'Job board type',
            'xmlPath' => 'JobBoard.JobBoard.JobboardType.JobboardType.Description.value.@content'
        ]
    ];

    public function setApiUsername($username) {
        $this->apiUsername = $username;
    }

    public function setApiPassword($password) {
        $this->apiPassword = $password;
    }

    public function setApiUrl($url) {
        $this->apiUrl = $url;
    }

    public function setApiAction($soapAction) {
        $this->soapAction = $soapAction;
    }

    public function getJobs($format = 'xml') {
        $raw = $this->retrieveRawData();

        if ($format != 'xml') {
            $raw = str_replace('<e:', '<', $raw);
            $raw = str_replace('</e:', '</', $raw);
            $raw = str_replace('<soap:', '<', $raw);
            $raw = str_replace('</soap:', '</', $raw);
            $raw = str_replace('<ns1:', '<', $raw);
            $raw = str_replace('</ns1:', '</', $raw);
            $raw = str_replace('<root:', '<', $raw);
            $raw = str_replace('</root:', '</', $raw);

            $rawArray = $this->XML2Array($raw);
            //var_dump($rawArray);die;

            if (isset($rawArray['Body']['findPartialEntitiesResponse']['Entities']['@attributes'])) {
                $output = $rawArray['Body']['findPartialEntitiesResponse']['Entities']['@attributes'];
                unset($rawArray['Body']['findPartialEntitiesResponse']['Entities']['@attributes']);

                if (isset($rawArray['Body']['findPartialEntitiesResponse']['Entities']['Entity'])) {
                    if ((int)$output['entityCount'] == 1) {
                        $entities = [$rawArray['Body']['findPartialEntitiesResponse']['Entities']['Entity']];
                    } else {
                        $entities = $rawArray['Body']['findPartialEntitiesResponse']['Entities']['Entity'];
                    }
                    foreach ($entities as $each) {
                        $item = [];
                        foreach ($this->projectionPaths as $projectionPath => $props) {
                            $item[$props['key']] = $this->valueForPath($each, $props['xmlPath']);
                        }
                        if ($item['ContestNumber']) {
                            $output['entities'][] = $item;
                        }
                    }
                }
                return $format == 'json' ? json_encode($output) : $output;
            }

            return false;
        }        
        return $raw;
    }

    public function setPaging($pagingSize, $pagingIndex) {
        $this->pagingSize = $pagingSize;
        $this->pagingIndex = $pagingIndex;
    }

    public function setSorting($fieldPath, $direction) {
        $this->sortField = $fieldPath;
        $this->sortAscending = strtolower($direction) == 'asc' ? 'true' : 'false';
    }

    public function addFilterAnd($key, $operator, $value) {
        $this->filtersAnd[] = [
            'key' => $key,
            'operator' => $operator,
            'value' => $value
        ];
    }

    public function addFilterOr($key, $operator, $value) {
        $this->filtersOr[] = [
            'key' => $key,
            'operator' => $operator,
            'value' => $value
        ];
    }

    public function addFilterAndOr($key, $operator, $value) {
        $this->filtersAndOr[] = [
            'key' => $key,
            'operator' => $operator,
            'value' => $value
        ];
    }

    public function getRequestXML() {
        // projections
        $projectionsPart = '';
        foreach($this->projectionPaths as $projectionPath => $props) {
            $projectionsPart .= '<ns2:projection><ns2:field path="' . $projectionPath . '"/></ns2:projection>';
        }

        // filtering
        $filtersPart = '';
        if (count($this->filtersAnd) || count($this->filtersOr)) {
            $filtersPart = '<ns2:filterings>';

            if (count($this->filtersAnd)) {                
                $filtersPart .= '<ns2:filtering>' . (count($this->filtersAnd) > 1 ? '<ns2:and>' : '');
                foreach ($this->filtersAnd as $props) {
                    $filtersPart .= '<ns2:' . $props['operator'] . '><ns2:field path="' . $props['key'] . '" /><ns2:string>' . $props['value'] . '</ns2:string></ns2:' . $props['operator'] . '>';
                }
                $filtersPart .= (count($this->filtersAnd) > 1 ? '</ns2:and>' : '') . '</ns2:filtering>';
            }

            if (count($this->filtersOr)) {
                $filtersPart .= '<ns2:filtering>' . (count($this->filtersOr) > 1 ? '<ns2:or>' : '');
                foreach ($this->filtersOr as $props) {
                    $filtersPart .= '<ns2:' . $props['operator'] . '><ns2:field path="' . $props['key'] . '" /><ns2:string>' . $props['value'] . '</ns2:string></ns2:' . $props['operator'] . '>';
                }
                $filtersPart .= (count($this->filtersOr) > 1 ? '</ns2:or>' : '') . '</ns2:filtering>';
            }

            if (count($this->filtersAndOr)) {
                $filtersPart .= '<ns2:filtering>' . (count($this->filtersAndOr) > 1 ? '<ns2:and><ns2:or>' : '');
                foreach ($this->filtersAndOr as $props) {
                    $filtersPart .= '<ns2:' . $props['operator'] . '><ns2:field path="' . $props['key'] . '" /><ns2:string>' . $props['value'] . '</ns2:string></ns2:' . $props['operator'] . '>';
                }
                $filtersPart .= (count($this->filtersAndOr) > 1 ? '</ns2:or></ns2:and>' : '') . '</ns2:filtering>';
            }

            $filtersPart .= '</ns2:filterings>';
        }

        $xml = '<?xml version="1.0" encoding="UTF-8"?>
            <SOAP-ENV:Envelope
                xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
                xmlns:ns1="http://www.taleo.com/ws/tee800/2009/01/find"
                xmlns:ns2="http://itk.taleo.com/ws/query">
                <SOAP-ENV:Body>
                    <ns1:findPartialEntities>
                        <ns1:mappingVersion>http://www.taleo.com/ws/art750/2006/12</ns1:mappingVersion>
                        <ns1:query>
                            <ns2:query projectedClass="SourcingRequest" alias="Alias Description">
                                <ns2:subQueries/>
                                <ns2:projections>
                                    ' . $projectionsPart . '
                                </ns2:projections>                    
                                <ns2:projectionFilterings />' . $filtersPart . '
                                <ns2:sortings>
                                    <ns2:sorting ascending="' . $this->sortAscending . '">
                                        <ns2:field path="' . $this->sortField . '" />
                                    </ns2:sorting>
                                </ns2:sortings>                                
                                <ns2:sortingFilterings />
                                <ns2:groupings />
                                <ns2:joinings />                                                                
                            </ns2:query>
                        </ns1:query>
                        <ns1:attributes>
                            <ns1:entry>
                                <ns1:key>pageindex</ns1:key>
                                <ns1:value>' . $this->pagingIndex . '</ns1:value>
                            </ns1:entry>
                            <ns1:entry>
                                <ns1:key>pagingsize</ns1:key>
                                <ns1:value>' . $this->pagingSize . '</ns1:value>
                            </ns1:entry>
                        </ns1:attributes>            
                    </ns1:findPartialEntities>
                </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>';
        return $xml;
    }

    public function retrieveRawData() {
            $xml = $this->getRequestXML();
            
            // perform request
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->apiUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            $headers = array();
            array_push($headers, "Content-Type: text/xml; charset=utf-8");
            array_push($headers, "Accept: text/xml");
            array_push($headers, "Cache-Control: no-cache");
            array_push($headers, "Pragma: no-cache");
            array_push($headers, 'SOAPAction: "' . $this->soapAction . '"');
            curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
            array_push($headers, "Content-Length: " . strlen($xml));
            if ($this->apiUsername && $this->apiPassword) {
                curl_setopt($ch, CURLOPT_USERPWD, $this->apiUsername . ':' . $this->apiPassword);
            }
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
            
        }


    private function XML2Array($xmlstr) {
        $doc = new DOMDocument();
        $doc->loadXML($xmlstr);
        $root = $doc->documentElement;
        $output = $this->DOMNode2Array($root);
        $output['@root'] = $root->tagName;
        return $output;
    }

    private function DOMNode2Array($node) {
        $output = array();
        switch ($node->nodeType) {
            case XML_CDATA_SECTION_NODE:
            case XML_TEXT_NODE:
                $output = trim($node->textContent);
                break;
            case XML_ELEMENT_NODE:
                for ($i=0, $m=$node->childNodes->length; $i<$m; $i++) {
                    $child = $node->childNodes->item($i);
                    $v = $this->DOMNode2Array($child);
                    if(isset($child->tagName)) {
                        $t = $child->tagName;
                        if(!isset($output[$t])) {
                            $output[$t] = array();
                        }
                        $output[$t][] = $v;
                    }
                    elseif($v || $v === '0') {
                        $output = (string) $v;
                    }
                }
                if($node->attributes->length && !is_array($output)) {
                    $output = array('@content'=>$output);
                }
                if(is_array($output)) {
                    if($node->attributes->length) {
                        $a = array();
                        foreach($node->attributes as $attrName => $attrNode) {
                            $a[$attrName] = (string) $attrNode->value;
                        }
                        $output['@attributes'] = $a;
                    }
                    foreach ($output as $t => $v) {
                        if(is_array($v) && count($v)==1 && $t!='@attributes') {
                            $output[$t] = $v[0];
                        }
                    }
                }
                break;
        }
        return $output;
    }

    private function valueForPath($array, $path) {
        $path = explode('.', $path); //if needed
        $path = "['" . implode("']['", $path) . "']";
        eval("\$result = isset(\$array{$path}) ? \$array{$path} : null;");
        return $result;
    }


}





