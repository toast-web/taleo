<?php

class UpdateCareersTask extends BuildTask {

    protected $title = 'Update careers from Taleo\'s API';

    protected $description = 'Update careers from Taleo\'s API and relate categories';

    private $records = 0;

    private $ids = [];

    public function run($request) {

        if ((bool)$request->getVar('wipe')) {
            DB::query('DELETE FROM TaleoJob');
        }

        $this->importPage(1);
        $this->relateCategories();
    }

    private function importPage($pageIndex) {

        $taleo = new Taleo;        
        $taleo->setApiUsername(Config::inst()->get('Taleo', 'api_username'));
        $taleo->setApiPassword(Config::inst()->get('Taleo', 'api_password'));
        $taleo->setApiUrl(Config::inst()->get('Taleo', 'api_url'));
        $taleo->setApiAction(Config::inst()->get('Taleo', 'api_action'));        
        
        $taleo->setPaging(100, $pageIndex);

        $jobBoardNumber = Config::inst()->get('Taleo', 'board_number');

        if ($jobBoardNumber) {
            $taleo->addFilterAnd('JobBoard,JobBoard.Number', 'equal', $jobBoardNumber);
        }

        $taleo->addFilterAnd('SourcingRequestStatus,Number', 'equal', '2'); // live jobs
        $taleo->addFilterAnd('Requisition,State,Number', 'equal', '13');    // not filled, not draft
        
        $taleo->setSorting('CreationDate', 'desc');


        try {            

            $data = $taleo->getJobs('array');
            
            if (isset($data['entities']) && isset($data['pageIndex']) && isset($data['pageCount'])) {

                echo 'Got page ' . $data['pageIndex'] . ' of ' . (int)$data['pageCount'] . PHP_EOL;

                foreach($data['entities'] as $each) {
                    if (isset($each['ContestNumber']) && $each['ContestNumber'] && isset($each['RequisitionNumber']) && $each['RequisitionNumber']) {
                        $job = TaleoJob::get()->filter('ContestNumber', $each['ContestNumber'])->first();

                        if (!$job) {
                            $job = new TaleoJob;
                            echo "Creating job " . $each['RequisitionNumber'] . PHP_EOL;
                        } else {
                            echo "Updating job " . $each['RequisitionNumber'] . PHP_EOL;
                        }

                        $job->ContestNumber = $each['ContestNumber'];
                        $job->RequisitionNumber = $each['RequisitionNumber'];
                        $job->CreationDate = isset($each['CreationDate']) ? $each['CreationDate'] : null;
                        $job->CloseDate = isset($each['CloseDate']) ? $each['CloseDate'] : null;
                        $job->OpenDate = isset($each['OpenDate']) ? $each['OpenDate'] : null;
                        $job->JobBoardNumber = isset($each['JobBoardNumber']) ? $each['JobBoardNumber'] : null;
                        $job->JobBoardType = isset($each['JobBoardType']) ? $each['JobBoardType'] : null;
                        $job->RequisitionState = isset($each['RequisitionState']) ? $each['RequisitionState'] : null;
                        $job->RequisitionStateNumber = isset($each['RequisitionStateNumber']) ? $each['RequisitionStateNumber'] : null;
                        $job->SourcingRequestStatus = isset($each['SourcingRequestStatus']) ? $each['SourcingRequestStatus'] : null;
                        $job->SourcingRequestNumber = isset($each['SourcingRequestNumber']) ? $each['SourcingRequestNumber'] : null;
                        $job->Title = isset($each['Title']) ? $each['Title'] : null;
                        $job->JobFieldCode = isset($each['JobFieldCode']) ? $each['JobFieldCode'] : null;
                        $job->JobFieldName = isset($each['JobFieldName']) ? $each['JobFieldName'] : null;
                        $job->JobFieldLevel = isset($each['JobFieldLevel']) ? $each['JobFieldLevel'] : null;
                        $job->JobFieldParentName = isset($each['JobFieldParentName']) ? $each['JobFieldParentName'] : null;
                        $job->OrganizationLevel = isset($each['OrganizationLevel']) ? $each['OrganizationLevel'] : null;
                        $job->OrganizationCode = isset($each['OrganizationCode']) ? $each['OrganizationCode'] : null;
                        $job->OrganizationName = isset($each['OrganizationName']) ? $each['OrganizationName'] : null;
                        $job->LocationCode = isset($each['LocationCode']) ? str_replace('  ', ' ', $each['LocationCode']) : null;
                        $job->LocationName = isset($each['LocationName']) ? str_replace('  ', ' ', $each['LocationName']) : null;
                        $job->LocationParentName = isset($each['LocationParentName']) ? str_replace('  ', ' ', $each['LocationParentName']) : null;                        
                        $job->Description = isset($each['Description']) ? $each['Description'] : null;
                
                        $this->ids[] = $job->write();

                        $this->records++;
                    }
                }
                
                // retrieve next page until no pages left
                if ((int)$data['pageIndex'] < (int)$data['pageCount']) {
                    sleep(1);
                    $this->importPage((int)$data['pageIndex'] + 1);
                } else {
                    echo 'Total records processed: ' . $this->records . PHP_EOL;

                    if (count($this->ids)) {
                        echo 'Now removing old jobs... ';
                        DB::query('DELETE FROM TaleoJob WHERE ID NOT IN (' . implode(',', $this->ids) . ')');
                        echo 'finished.' . PHP_EOL;
                    }

                }

            } else {
                echo 'ERROR: Got empty result from API' . PHP_EOL;
            }


        } catch(Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }
        
    }


    private function relateCategories() {        
        foreach(TaleoJob::get() as $job) {
            $job->SubCategoryID = 0;
            $job->CategoryID = 0;

            if ($job->JobFieldParentName != 'Clinical' && $job->JobFieldParentName != 'Non Clinical') {

                if ($jobCategory = TaleoJobCategory::get()->filter(['Title' => $job->JobFieldParentName])->first()) {
                    $job->CategoryID = $jobCategory->ID;
                    // can only have a sub-category if there's a category
                    if ($jobSubCategory = TaleoJobSubCategory::get()->filter(['Title' => $job->JobFieldName, 'ParentID' => $jobCategory->ID])->first()) {
                        $job->SubCategoryID = $jobSubCategory->ID;
                    }
                }

            } else {

                if ($jobCategory = TaleoJobCategory::get()->filter(['Title' => $job->JobFieldName])->first()) {
                    $job->CategoryID = $jobCategory->ID;
                }
            }

            $job->write();
        }

    }

}