<?php

class TaleoJobCategory extends DataObject {

    private static $singular_name = 'Job category';

    private static $plural_name = 'Job categories';

    private static $db = array(
        'CategoryType' => 'Enum("Clinical,Non Clinical")',
        'Title' => 'Varchar(255)',
        'Disabled' => 'Boolean'
    );

    private static $has_one = array(
        'Image' => 'Image'
    );

    private static $has_many = array(
        'SubCategories' => 'JobSubCategory',
        'Jobs' => 'Job'
    );

    private static $summary_fields = array(
        'CategoryType',
        'Title',
        'Thumbnail' => 'Thumbnail',
        'Status' => 'Status'
    );

    private static $searchable_fields = array(
        'Title'
    );

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Main', CheckboxField::create('Disabled', 'Mark as Inactive'));
        return $fields;
    }

	public function Thumbnail() {
		if ($this->Image()->exists()) {
			if ($image = $this->Image()->SetHeight(100)) {
				if ($image->hasMethod('getTag')) {
					return DBField::create_field('HTMLText', $this->Image()->SetHeight(100)->getTag());
				}
			}
		}
	}

    public function Active() {
        return !$this->Disabled;
    }

    public function Status() {
        return $this->Active() ? 'Active' : 'Inactive';
    }

    public function canDelete($member = null) {
        return false;
    }

}
