<?php

class TaleoJobSubCategory extends DataObject {

    private static $singular_name = 'Job sub-category';

    private static $plural_name = 'Job sub-categories';

    private static $db = array(
        'Title' => 'Varchar(255)',
        'Disabled' => 'Boolean'
    );

    private static $has_one = array(
        'Parent' => 'TaleoJobCategory',
        'Image' => 'Image',
    );

    private static $has_many = array(
        'Jobs' => 'Job'
    );

    private static $summary_fields = array(
        'Parent.Title' => 'Category',
        'Title' => 'Title',
        'Thumbnail' => 'Thumbnail',
        'Status' => 'Status'
    );

    private static $searchable_fields = array(
        'Title'
    );

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Main', CheckboxField::create('Disabled', 'Mark as Inactive'));
        return $fields;
    }

    public function Thumbnail() {
		if ($this->Image()->exists()) {
			if ($image = $this->Image()->SetHeight(100)) {
				if ($image->hasMethod('getTag')) {
					return DBField::create_field('HTMLText', $this->Image()->SetHeight(100)->getTag());
				}
			}
		}
	}

    public function Active() {
        return !$this->Disabled;
    }

    public function Status() {
        return $this->Active() ? 'Active' : 'Inactive';
    }

    public function canDelete($member = null) {
        return false;
    }

}
