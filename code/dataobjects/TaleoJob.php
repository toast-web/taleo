<?php

class TaleoJob extends DataObject {

    private static $apply_link = false;

    private static $singular_name = 'Job';

    private static $plural_name = 'Jobs';

    private static $db = array(
        'RequisitionNumber' => 'Varchar(50)',
        'ContestNumber' => 'Varchar(50)',
        'CreationDate' => 'SS_Datetime',
        'CloseDate' => 'SS_Datetime',
        'OpenDate' => 'SS_Datetime',
        'JobBoardNumber' => 'Varchar(50)',
        'JobBoardType' => 'Varchar(50)',
        'RequisitionState' => 'Varchar(50)',
        'RequisitionStateNumber' => 'Varchar(50)',
        'SourcingRequestStatus' => 'Varchar(50)',
        'SourcingRequestNumber' => 'Varchar(5)',
        'Title' => 'Varchar(255)',
        'JobFieldCode' => 'Varchar(50)',
        'JobFieldName' => 'Varchar(100)',
        'JobFieldParentName' => 'Varchar(100)',
        'JobFieldLevel' => 'Varchar(3)',
        'OrganizationLevel' => 'Varchar(3)',
        'OrganizationCode' => 'Varchar(50)',
        'OrganizationName' => 'Varchar(255)',
        'LocationCode' => 'Varchar(255)',
        'LocationName' => 'Varchar(255)',
        'LocationParentName' => 'Varchar(255)',
        'Description' => 'HTMLText'
    );

    private static $has_one = array(
        'Category' => 'TaleoJobCategory',
        'SubCategory' => 'TaleoJobSubCategory',
        'Subsite' => 'Subsite'
    );

    private static $summary_fields = array(
        'Type' => 'CategoryType',
        'Category' => 'Category.Title',
        'SubCategory' => 'SubCategory.Title',
        'ContestNumber',
        'Title',
        'OrganizationName',
        'OrganizationCode',
        'LocationName'
    );

    private static $searchable_fields = array(
        'ContestNumber',
        'Title',
        'OrganizationName',
        'OrganizationCode'
    );

    public function CategoryType() {
        if ($this->Category()->exists()) {
            $type = $this->Category()->CategoryType;
        } elseif ($this->SubCategory()->exists() && $this->SubCategory()->Parent()->exists()) {
            $type = $this->SubCategory()->Parent()->CategoryType;
        } else {
            $type = $this->JobFieldName;
        }
        return DBField::create_field('Text', $type);
    }

    public function Image() {
        if ($this->SubCategory()->exists() && $this->SubCategory()->Image()->exists()) {
            return $this->SubCategory()->Image();
        } elseif ($this->Category()->exists() && $this->Category()->Image()->exists()) {
            return $this->Category()->Image();
        }
    }

    public function RelatedJobs() {
        if ($this->SubCategory()->exists()) {
            return Job::get()
                ->where('CloseDate >= NOW()')
                ->where('ID != ' . $this->ID)
                ->where('SubCategoryID = ' . $this->SubCategoryID)
                ->sort('MD5(RAND())');
        }
    }

    public function ApplyLink() {
        $applyLink = Config::inst()->get('TaleoJob', 'apply_link');
        if ($applyLink) {
            $applyLink = str_replace('{REQUISITION}', $this->RequisitionNumber, $applyLink);
            return Convert::raw2xml($applyLink);
        } else {
            return '#';
        }
    }
    
    public function canCreate($member = null) {
        return Director::is_cli();
	}

    public function canEdit($member = null) {
		return Director::is_cli();
	}

    public function canDelete($member = null) {
		return Director::is_cli();
	}

}
