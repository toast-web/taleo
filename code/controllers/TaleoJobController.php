<?php

class TaleoJobController extends Controller
{

    var $types = array(
        '1' => 'Clinical',
        '2' => 'Non Clinical'
    );

    private static $allowed_actions = [
        'jobs',
        'types',
        'categories',
        'subcategories',
        'locations'
    ];

    public function getTypes() {
        $output = new ArrayList;
        foreach($this->types as $id => $title) {
            $output->push(new ArrayData(array(
                'ID' => $id,
                'Title' => $title
            )));
        }
        return $output;
    }

    public function getCategories($typeId = null) {
        $categories = TaleoJobCategory::get()->filter('Disabled', false)->sort('Title');
        if ($typeId !== null) {
            $categories = $categories->filter('CategoryType', $this->types[(int)$typeId]);
        }
        return $categories;
    }

    public function getSubCategories($categoryId = null) {
        $subCategories = TaleoJobSubCategory::get()->filter('Disabled', false)->sort('Title');
        if ($categoryId !== null) {
            $subCategories = $subCategories->filter('ParentID', (int)$categoryId);
        }
        return $subCategories;
    }

    public function getJobs($keyword = null, $typeId = null, $categoryId = null, $subCategoryId = null, $locationId = null) {
        $jobs = TaleoJob::get()
            ->sort('CreationDate', 'DESC');

        if (trim($keyword)) {
            $keyword = Convert::raw2sql($keyword);
            $jobs = $jobs->where('
                LOWER(ContestNumber) = \'' . strtolower($keyword) . '\' OR
                Title LIKE \'%' . $keyword . '%\' OR
                Description LIKE \'%' . $keyword . '%\'
            ');
        }

        if ($subCategoryId) {            
            if ($subcategory = TaleoJobSubCategory::get()->filter(['Disabled' => false, 'ID' => (int)$subCategoryId])->first()) {
                $jobs = $jobs->where('SubCategoryID = ' . (int)$subCategoryId);
                //$jobs = $jobs->where('JobFieldName = \'' . $subcategory->Title . '\'');
            }

        } elseif ($categoryId) {
            $jobs = $jobs->where('CategoryID = ' . (int)$categoryId);
        } elseif ($typeId) {
            $categoryIds = [];
            $subCategoryIds = [];
            foreach(TaleoJobCategory::get()->filter(['Disabled' => false, 'CategoryType' => $this->types[(int)$typeId]]) as $category) {
                $categoryIds[] = $category->ID;
                foreach($category->SubCategories() as $subCategory) {
                    $subCategories[] = $subCategory->ID;
                }
            }
            $wheres = [];
            if (count($categoryIds)) {
                $wheres[] = 'CategoryID IN (' . implode(',', $categoryIds) . ')';
            }
            if (count($subCategoryIds)) {
                $wheres[] = 'SubCategoryID IN (' . implode(',', $subCategoryIds) . ')';
            }
            if (count($wheres)) {
                $jobs = $jobs->where(implode(' OR ', $wheres));
            }
        }

        if ($locationId) {
            if ($locationId != 'Auckland Region') { // Show all if selecting Auckland Region
                $jobs = $jobs->where('
                    REPLACE(LocationName, \'  \', \' \') = \'' . Convert::raw2sql($locationId) . '\' OR 
                    REPLACE(LocationParentName, \'  \', \' \') = \'' . Convert::raw2sql($locationId) . '\'
                ');
            }
        }

        return $jobs;
    }

    public function getLocations($parentId = false) {
        $output = new ArrayList;
        foreach(array_flip($this->getJobs()->column('LocationName')) as $key => $value) {
            $output->push(new ArrayData(array(
                'ID' => str_replace('  ', ' ', $key),
                'Title' => str_replace('  ', ' ', $key)
            )));
        }
        return $output->sort('Title');
    }

    public function jobs(SS_HTTPRequest $request) {
        $data = $this->getJobs($request->getVar('keyword'), $request->getVar('type'), $request->getVar('category'), $request->getVar('subcategory'));
        foreach($data as $job) {
            $output[] = $job->toMap();
        }
        return $this->getStandardJsonResponse($output);
    }

    public function types(SS_HTTPRequest $request) {
        $output = array();
        foreach($this->getTypes() as $type) {
            $output[] = $type->toMap();
        }
        return $this->getStandardJsonResponse($output);
    }

    public function categories(SS_HTTPRequest $request) {
        $output = array();
        foreach($this->getCategories($request->getVar('type')) as $category) {
            $totalJobs = $category->Jobs()->count();
            if ($totalJobs) {
                $output[] = array(
                    'ID' => $category->ID,
                    'Title' => $category->Title . ' (' . $totalJobs . ')',
                    'Type' => $category->CategoryType,
                    'Image' => $category->Image()->exists() ? $category->Image()->URL : null
                );
            }
        }
        return $this->getStandardJsonResponse($output);
    }

    public function subcategories(SS_HTTPRequest $request) {
        $output = array();
        foreach($this->getSubCategories($request->getVar('category')) as $subcategory) {
            $totalJobs = $subcategory->Jobs()->count();
            if ($totalJobs) {
                $parent = $subcategory->Parent();
                $output[] = array(
                    'ID' => $subcategory->ID,
                    'Title' => $subcategory->Title . ' (' . $totalJobs . ')',
                    'ParentID' => $parent->exists() ? $parent->ID : null,
                    'ParentTitle' => $parent->exists() ? $parent->Title : null,
                    'Image' => $subcategory->Image()->exists() ? $subcategory->Image()->URL : ($subcategory->Parent()->exists() && $subcategory->Parent()->Image()->exists() ? $subcategory->Parent()->Image()->URL : null)                
                );
            }
        }
        return $this->getStandardJsonResponse($output);
    }

    public function locations(SS_HTTPRequest $request) {
        $output = array();
        foreach($this->getLocations() as $location) {
            $output[] = array(
                'ID' => $location->ID,
                'Title' => $location->Title
            );
        }
        return $this->getStandardJsonResponse($output);
    }

    public function getStandardJsonResponse($data, $method = 'json', $message = '', $code = 200, $status = 'success')
    {
        $elapsed = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
        $response = [
            'request' => $this->getRequest()->httpMethod(),
            'status'  => $status, // success, error
            'method'  => $method,
            'elapsed' => number_format($elapsed * 1000, 0) . 'ms',
            'message' => $message,
            'code'    => $code,
            'data'    => $data
        ];
        return json_encode($response);
    }

}
