<?php

class TaleoSiteConfigExtension extends DataExtension
{

    private static $has_one = [
        'JobCategoryImage' => 'Image'
    ];

    public function updateCMSFields(FieldList $fields)
    {

        $fields->addFieldsToTab('Root.Taleo', array(
            UploadField::create('JobCategoryImage', 'Fallback Image for Categories'),
            LiteralField::create('', '
                <div class="message">
                    <p>This will open a new window which must be left open until the update completes.</p>
                    <p><a href="/dev/tasks/UpdateCareersTask" target="_blank">Click here to update jobs now</a></p>
                </div>
            ')
        ));

    }
}