Silverstripe module to pull jobs from Taleo SOAP API.

Requirements: Silverstripe 3

------------------------------------

Under "Taleo Jobs" (CMS):

1. Upload CSV with Categories
2. Upload CSV with Subcategories
3. Run task /dev/tasks/UpdatCareersTask
4. Create a page "Taleo Page"
