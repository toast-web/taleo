
<h1>$Title.XML</h1>

<% if $Job %>
    <% include TaleoJobDetails Job=$Job, Top=$Top %>
<% else %>

    <% include TaleoJobList PaginatedJobs=$PaginatedJobs, Top=$Top %>
<% end_if %>
